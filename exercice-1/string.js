function ucfirst(str){
	if(typeof str !== "string" || !str) return '';

	return str[0].toUpperCase() + str.substring(1);
}

console.log(ucfirst("test"));
console.log(ucfirst("Test"));
console.log(ucfirst(" test"));
console.log(ucfirst("test Test tst"));
console.log(ucfirst(""));
console.log(ucfirst(null));
console.log(ucfirst({}));

function capitalize(str){
	if(typeof str !== "string" || !str) return '';

	return str.toLowerCase().split(" ").map(function(item){
		return ucfirst(item);
	}).join(" ");
}


console.log(capitalize("test"));
console.log(capitalize("Test"));
console.log(capitalize(" test"));
console.log(capitalize("test Test tst"));
console.log(capitalize(""));
console.log(capitalize(null));
console.log(capitalize({}));

function camelCase(str){
	if(typeof str !== "string" || !str) return '';

	return str.toLowerCase().split(/[^a-zA-Z0-9]/).map(function(item){
		return ucfirst(item);
	}).join("");
	
}

console.log(camelCase("hello world"));
console.log(camelCase("ToggleCase is_the coolest"));

function snake_case(str){
	if(typeof str !== "string" || !str) return '';

	//return str.toLowerCase().split(/[^a-zA-Z0-9]/).join('_');
	return str.toLowerCase().replace(/[^a-zA-Z0-9]/g,'_');

	//faut toujours mettre un g quand on utilise replace()
	//pour dire qu'on les veut tous
}

console.log(snake_case("test"));
console.log(snake_case("Test"));
console.log(snake_case(" test"));
console.log(snake_case("test Test tst"));
console.log(snake_case(""));
console.log(snake_case(null));
console.log(snake_case({}));
console.log(snake_case('toggleCase is the coolest'));

function leet(str){
	if(typeof str !== "string" || !str) return '';
	alphabets = { a: "4", e: "3", i: '1', o: '0', u: '(_)', y: '7'};
	for (var i = 0; i < str.length; i++) {
		//autre méthode
		// let reg = new RegExp(i, "gi");
		//str = str.replace(regn obj[i]);

		//autre methode ( version optimale )
		//return str.replace(/[aeiouy]/gi, function(item){
		//	switch(item){
			//case 'a':
			//case 'A' :
				//return 4;
		//}
		//})
		if(alphabets[str[i].toLowerCase()]) str = str.replace(str[i],alphabets[str[i]]);
	}
	return str;

}
console.log(leet("anaconda"));

function verlan(str){
	if(typeof str !== "string" || !str) return '';
	return str.split(' ').map(function(item){
		return item.split('').reverse().join('');
	}).join(' ');
}

console.log(verlan("test"));
console.log(verlan("Test"));
console.log(verlan(" test"));
console.log(verlan("hello world"));

function yoda(str){
	if(typeof str !== "string" || !str) return '';

	return str.split(' ').reverse().join(' ');
}

console.log(yoda("hello world hihi"));

function vig1(str, cle){
	if(typeof str !== "string" || !str) return '';
	code = 0;
	res = [];
	decalage = 0;
	lettre = "";
	for(var i = 0; i<str.length;i++){
		//if(str[i] === '/[a-zA-Z]/'){
		code = str.charCodeAt(i)-97;
		decalage = cle.charCodeAt(i%cle.length)-97;
		lettre = String.fromCharCode(65+(code+decalage)%26);
		res = res + lettre;
		//}
		
	}
	return res.toLowerCase();
}
console.log(vig('wikipedia','crypto'));


function vig(str, code) {
	if(typeof str !== "string" || !str) return '';

	while(code.length<str.length) {
		code += code;
	}
	let codeIndex = 0;
	return str.split('').map(function(char) {
		//Position dans l'alphabet du car du message
		const charCode = char.charCodeAt(0) - "a".charCodeAt(0);
		// Verifie car is alpha
		if(charCode < 0 || charCode >25) return char;
		//Position dans l'alphabet du car du code
		const codeCode = code[codeIndex++].charCodeAt(0) - "a".charCodeAt(0);
		//on applique vigenere
		const cryptedCode = (charCode + codeCode) % 26;
		// position du car codé dans la table ASCII
		const cryptedChar = cryptedCode + "a".charCodeAt(0);
		// On récupère le code
		return String.fromCharCode(cryptedChar);
	}).join('');
}
console.log(vig('wikipedia','crypto'));

//V1 arg 1 = var , arg 2 = string => les array et null
//V2 agr 1 = ce quon veut + arg2 = tjrs valide => non null

function type_check_v1(char, string) {
	if(typeof(char) === string) {
		return true;
	}
	else return false;
}

console.log(type_check_v1(4,'number'));
console.log(type_check_v1('hello','number'));
console.log(type_check_v1('hello','string'));





