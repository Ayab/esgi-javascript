function type_check_v11(char, string) {
	if(typeof(char) === string) {
		bool = true;
		if(char.constructor === string || isFinite(char) || char instanceof String || {}.toString.call(char) || isNAN(char) === string) {
			bool = true;
		}
	}
	else bool = false;

	return bool;
}

function type_check_v1(value, type) {
	switch(typeof value) {
		case 'object':
			if(Array.isArray(value)) return type === "array";
			if(value === null) return type === "null";
		default:
			return typeof value === type;	
	}	
}

console.log(type_check_v1(4,'number'));
console.log(type_check_v1('hello','number'));
console.log(type_check_v1(null,'null'));